# PEEPS

A mock social media app to demo React and CSS

## Tech Rationale

Since there is hardly any static content, a plain React app works well here.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Requirements are simple enough to use plain CSS

## Building

Uses current node lts (v12.19)

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />

## Future Design Considerations

Although it would be unfrequently used, adding a two column layout would be trivial and would simplify the code and make the app more responsive.

## Todo

* add search bar
* add card placeholders 
* refine transitions/animations
* restrict grid size to make row and column gaps consistent
* factor out colors into custom properties
* refine iOS safari compatability (disable scrollbar on modal open)