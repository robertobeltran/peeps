import React, {useState, useEffect} from 'react'; 
import './User.css';
import PlusSVG from './svgs/PlusSVG';
import PhoneSVG from './svgs/PhoneSVG';
import CellSVG from './svgs/CellSVG';

function User({user}) {
  const [expanded, setExpanded] = useState(false);
  useEffect(() => {
    window.addEventListener('scroll', () => {
      document.documentElement.style.setProperty('--scroll-y', `${window.scrollY}px`);
    });
  }, [])
  useEffect(() => {
    const userSize = "351px"
    if (expanded) {
      document.body.style.overflow = 'hidden';
      const scrollY = document.documentElement.style.getPropertyValue('--scroll-y');
      document.body.style.top = `-${scrollY}`;
      document.body.style.marginBottom = userSize;
    }
    else {
      document.body.style.overflow = 'unset';
      document.body.style.position = '';
      const scrollY = document.body.style.top;
      window.scrollTo(0, parseInt(scrollY || '0') * -1);
      document.body.style.marginBottom = "0px"
    }
  }, [expanded]);
  function expansionHandler() {
    setExpanded(!expanded);
  }
  return (
    <div className={`user-backdrop ${expanded ? "expanded" : ""}`}>
      <div className={`user ${expanded ? "expanded" : ""}`}>
        <div className="user-content">
          <img src={user.picture.large} alt="" className="user-image" onClick={expansionHandler}/>
          <div>
            <p className="user-name">{user.name.first} {expanded && user.name.last} </p>
          </div>
        </div>
        <PlusSVG onClick={expansionHandler} />
        <div className="user-details">
          <p className="user-age">{user.dob.age} Years Old</p>
          <p>{user.location.city}, {user.location.state}</p>
          <p>{user.email}</p>
          <p>
            <PhoneSVG />
            {user.phone}
          </p>
          <p>
            <CellSVG />
            {user.cell}
          </p>
        </div>
      </div>
    </div>
  )
}

export default User;
