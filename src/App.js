import React, {useEffect, useState} from 'react'; 
import User from './User'
import { v4 as uuidv4 } from 'uuid';
import './App.css';

function App() {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    async function getRandomUsers() {
      // only US to not worry about i18n, excluding login to speed up per docs
      const url = 'https://randomuser.me/api/?nat=US&exc=login&results=12'
      fetch(url)
        .then(response => response.json())
        .then(data => setUsers(data.results));
    }
    getRandomUsers();
  }, []);
  useEffect(() => {
    console.log(users)
  }, [users])
  return (
    <div className="users">
    {
      users.map(user => <User key={uuidv4()} user={user}/>)
    }
    </div>
  );
}

export default App;
